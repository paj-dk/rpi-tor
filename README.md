# rpi-tor
### Tor node for Raspberry PI deployed by Docker

### Usage
Edit torrc file to match your need. See git repo for examples
``` bash
# Run a tor relay
$ docker run -d -v torrc:/etc/tor/torrc -p 9001:9001 pajdk/rpi-tor:latest
# Run a tor relay and will also open a SOCKS connect on 9050
$ docker run -d -v torrc:/etc/tor/torrc -p 9001:9001 -p 9050:9050 \
     pajdk/rpi-tor:latest
# Open up a Tor SOCKS proxy on the Raspberry Pi
$ docker run -d -v torrc-bridge:/etc/tor/torrc -p 9050:9050 pajdk/rpi-tor:latest
```
