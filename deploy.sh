#!/bin/bash

docker tag pajdk/rpi-tor:${1} pajdk/rpi-tor:latest
docker push pajdk/rpi-tor:${1}
docker push pajdk/rpi-tor:latest
git add Dockerfile
git commit -m "Update tor node ${1}"
git pull
git push
git tag ${1}
git push --tags
